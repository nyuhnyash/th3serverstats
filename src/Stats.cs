namespace Th3ServerStats
{
    public class ServerInfo
    {
        public string Name { get; set; }

        public string Version { get; set; }

        public WorldInfo World { get; set; }

        public PlayersInfo Players { get; set; }
    }
}
