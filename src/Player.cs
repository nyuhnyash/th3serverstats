namespace Th3ServerStats
{
    public class Player
    {
        public string Name { get; set; }

        public float Ping { get; set; }

        public Player(string name, float ping)
        {
            Name = name;
            Ping = ping;
        }
    }
}