﻿using System.Collections.Generic;


namespace Th3ServerStats
{
    public class PlayersInfo
    {
        public int Online { get; set; }
        public int Max { get; set; }
        public IList<Player> List { get; set; }
    }
}
