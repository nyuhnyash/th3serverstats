namespace Th3ServerStats
{
    public class Th3ServerStatsConfig
    {
        public int? Port { get; set; }

        public string Host { get; set; } = "localhost";

        public string Endpoint { get; set; } = "stats";
    }
}